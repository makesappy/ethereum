package com.nous.ethereum.androidApp

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import com.nous.ethereum.shared.databaseDriverFactory
import com.nous.ethereum.shared.db.DatabaseDriverFactory
import com.nous.ethereum.shared.usecase.FetchEthAmountAndValueUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

const val UPDATE_ACTION = "updateAction"
const val WIDGET_ID = "w_id"

class EthWidget : AppWidgetProvider() {

    var widgetManager: AppWidgetManager? = null

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        reInstantiateDbDriverIfNeeded(context)
        widgetManager = appWidgetManager
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {}

    override fun onDisabled(context: Context) {}

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)
        if (intent != null && intent.action == UPDATE_ACTION) {
            context?.let {
                reInstantiateDbDriverIfNeeded(it)
                updateViews(it, intent.getIntExtra(WIDGET_ID, 0))
            }
        }
    }

    private fun updateAppWidget(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetId: Int
    ) {
        val views = updateViews(context, appWidgetId)

        views.setOnClickPendingIntent(
            R.id.root,
            PendingIntent.getBroadcast(
                context,
                0,
                Intent(context, EthWidget::class.java).apply {
                    action = UPDATE_ACTION
                    putExtra(WIDGET_ID, appWidgetId)
                },
                0
            )
        )

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views)
    }

    private fun updateViews(
        context: Context,
        widgetId: Int
    ): RemoteViews {
        val views = RemoteViews(context.packageName, R.layout.eth_widget)
        GlobalScope.launch(Dispatchers.IO) {
            val eth = FetchEthAmountAndValueUseCase().doWork()
            views.setTextViewText(R.id.tvWidget, eth?.value?.toInt()?.toString() ?: "0")
            reInstantiateWidgetManagerIfNeeded(context)
            withContext(Dispatchers.Main) {
                widgetManager?.updateAppWidget(widgetId, views)
            }
        }
        return views
    }

    private fun reInstantiateDbDriverIfNeeded(ctx: Context) {
        if (databaseDriverFactory == null) {
            databaseDriverFactory = DatabaseDriverFactory(ctx)
        }
    }

    private fun reInstantiateWidgetManagerIfNeeded(ctx: Context) {
        if (widgetManager == null) {
            widgetManager = AppWidgetManager.getInstance(ctx)
        }
    }
}