package com.nous.ethereum.androidApp

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.nous.ethereum.androidApp.databinding.ActivityMainBinding
import com.nous.ethereum.shared.databaseDriverFactory
import com.nous.ethereum.shared.db.DatabaseDriverFactory
import com.nous.ethereum.shared.viewModel.MainViewModel
import kotlinx.coroutines.flow.collect

@Suppress("EXPERIMENTAL_API_USAGE")
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var viewModel: MainViewModel

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        databaseDriverFactory = DatabaseDriverFactory(this)

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        lifecycleScope.launchWhenStarted {
            viewModel.viewState.collect {
                binding.tvCurrentAmount.text = it.amount.toString()
                binding.tvCurrentValue.text = it.value.toInt().toString()
            }
        }

        binding.btnSet.setOnClickListener {
            viewModel.setAmountAndValue(
                getAmount(),
                binding.tvCurrentValue.text.toString().toDouble()
            )
        }
        // reset my current amount
        binding.btnSet.setOnLongClickListener {
            viewModel.setAmountAndValue(
                2.07000493,
                binding.tvCurrentValue.text.toString().toDouble()
            )
            true
        }
    }

    private fun getAmount(): Double {
        val etText = binding.etAmount.text.toString()
        val tvText = binding.tvCurrentAmount.text.toString()
        return if (etText.isNotBlank()) {
            etText.toDouble()
        } else {
            tvText.toDouble()
        }
    }
}