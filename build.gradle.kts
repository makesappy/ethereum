buildscript {
    val kotlin_version by extra("1.4.10")
    repositories {
        gradlePluginPortal()
        jcenter()
        google()
        mavenCentral()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.21")
        classpath("com.android.tools.build:gradle:4.1.1")
        classpath("com.squareup.sqldelight:gradle-plugin:1.4.4")
        classpath("org.koin:koin-gradle-plugin:3.0.0-alpha-4")
    }
}
group = "com.nous.ethereum"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}
