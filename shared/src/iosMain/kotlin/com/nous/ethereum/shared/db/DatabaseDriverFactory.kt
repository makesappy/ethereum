package com.nous.ethereum.shared.db

import com.nous.ethereum.shared.cache.EthereumDatabase
import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.drivers.native.NativeSqliteDriver

actual class DatabaseDriverFactory {
    actual fun createDriver(): SqlDriver =
        NativeSqliteDriver(EthereumDatabase.Schema, "ethereum.db")
}