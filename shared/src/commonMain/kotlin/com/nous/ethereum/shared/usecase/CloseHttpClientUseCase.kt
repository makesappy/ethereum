package com.nous.ethereum.shared.usecase

import com.nous.ethereum.shared.network.EthereumRemoteSource

class CloseHttpClientUseCase {
    private val remoteSource = EthereumRemoteSource()
    operator fun invoke() = remoteSource.closeHttpClient()
}