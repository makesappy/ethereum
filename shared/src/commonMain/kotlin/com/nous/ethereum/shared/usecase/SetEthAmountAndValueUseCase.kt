package com.nous.ethereum.shared.usecase

import com.nous.ethereum.shared.repository.EthereumRepository

class SetEthAmountAndValueUseCase {
    private val repository = EthereumRepository()
    operator fun invoke(ethAmount: Double, ethValue: Double) =
        repository.setAmountAndValue(ethAmount, ethValue)
}