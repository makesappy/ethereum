package com.nous.ethereum.shared.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class UsdConversionRate(val rates: Rates, val base: String, val date: String)

@Serializable
data class Rates(
    @SerialName("CZK")
    val czk: Double
)