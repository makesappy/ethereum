package com.nous.ethereum.shared.db

import com.nous.ethereum.shared.cache.EthereumDatabase
import com.nous.ethereum.shared.databaseDriverFactory
import com.nous.ethereum.shared.model.EthAmountAndValue
import com.squareup.sqldelight.db.SqlDriver

class EthereumLocalSource {
    private val driver: SqlDriver = databaseDriverFactory?.createDriver()!!
    private val db = EthereumDatabase(driver).ethereumDatabaseQueries

    fun wipe() {
        db.wipe()
    }

    fun insertAmount(amount: Double,value:Double) {
        db.insert(amount.toString(),value.toString())
    }

    fun select(): EthAmountAndValue? {
        return try {
            db.select(::mapToDomain).executeAsOneOrNull()
        }catch (e: Exception){
            println(e)
            null
        }
    }

    private fun mapToDomain(
        id: Long,
        amount: String,
        value: String
    ) = EthAmountAndValue(amount.toDouble(), value.toDouble())
}