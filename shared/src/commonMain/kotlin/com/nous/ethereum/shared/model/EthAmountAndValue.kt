package com.nous.ethereum.shared.model

data class EthAmountAndValue(
    val amount: Double,
    val value: Double
)