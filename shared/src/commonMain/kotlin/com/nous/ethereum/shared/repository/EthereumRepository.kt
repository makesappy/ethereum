package com.nous.ethereum.shared.repository

import com.nous.ethereum.shared.db.EthereumLocalSource
import com.nous.ethereum.shared.model.EthAmountAndValue
import com.nous.ethereum.shared.network.EthereumRemoteSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class EthereumRepository {
    private val remoteSource = EthereumRemoteSource()
    private val localSource = EthereumLocalSource()

    suspend fun fetchAmountAndValue(): EthAmountAndValue? {
        val local = localSource.select()
        local ?: return null
        val convertedValue = checkRemoteRates(local.amount, local.value)
        localSource.insertAmount(local.amount, convertedValue)
        return EthAmountAndValue(local.amount, convertedValue)
    }

    fun setAmountAndValue(ethAmount: Double, ethValue: Double) {
        GlobalScope.launch(Dispatchers.Default) {
            val convertedValue = checkRemoteRates(ethAmount, ethValue)
            localSource.insertAmount(ethAmount, convertedValue)
        }
    }

    private suspend fun checkRemoteRates(
        ethAmount: Double,
        ethValue: Double
    ): Double {
        val currentEthRate = remoteSource.getActualEthereumUsdRate()
        val currentConversionRate = remoteSource.getUsdToCzkConversionRate()

        return if (currentEthRate != null && currentConversionRate != null) {
            ethAmount * currentEthRate * currentConversionRate
        } else {
            ethValue
        }
    }
}