package com.nous.ethereum.shared.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class EthValue(
    @SerialName("USD")
    val usd: Double
)