package com.nous.ethereum.shared.network

import com.nous.ethereum.shared.model.EthValue
import com.nous.ethereum.shared.model.UsdConversionRate
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*

class EthereumRemoteSource {
    private val client = HttpClient {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }

    suspend fun getActualEthereumUsdRate(): Double? {
        return try {
            client.get<EthValue>("https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD").usd
        } catch (e: Exception) {
            println(e)
            null
        }
    }

    suspend fun getUsdToCzkConversionRate(): Double? {
        return try {
            client.get<UsdConversionRate>("https://api.exchangeratesapi.io/latest?base=USD&symbols=CZK").rates.czk
        } catch (e: Exception) {
            println(e)
            null
        }
    }

    fun closeHttpClient() {
        client.close()
    }
}