package com.nous.ethereum.shared

import com.nous.ethereum.shared.db.DatabaseDriverFactory

expect var databaseDriverFactory: DatabaseDriverFactory?