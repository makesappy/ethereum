package com.nous.ethereum.shared.usecase

import com.nous.ethereum.shared.repository.EthereumRepository

class FetchEthAmountAndValueUseCase {
    private val repository = EthereumRepository()
    suspend fun doWork() = repository.fetchAmountAndValue()
}