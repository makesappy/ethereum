package com.nous.ethereum.shared.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nous.ethereum.shared.model.EthAmountAndValue
import com.nous.ethereum.shared.usecase.CloseHttpClientUseCase
import com.nous.ethereum.shared.usecase.FetchEthAmountAndValueUseCase
import com.nous.ethereum.shared.usecase.SetEthAmountAndValueUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class MainViewModel : ViewModel() {
    private val getEthAmountAndValueUseCase = FetchEthAmountAndValueUseCase()
    private val setEthAmountUseCase = SetEthAmountAndValueUseCase()
    private val closeHttpClientUseCase = CloseHttpClientUseCase()
    private val _viewState = MutableStateFlow(EthAmountAndValue(0.0, 0.0))
    val viewState: StateFlow<EthAmountAndValue> = _viewState

    init {
        updateValue()
    }

    fun setAmountAndValue(ethAmount: Double, ethValue: Double) {
        setEthAmountUseCase(ethAmount, ethValue).also {
            updateValue()
        }
    }

    private fun updateValue() {
        viewModelScope.launch(Dispatchers.IO) {
            _viewState.value = getEthAmountAndValueUseCase.doWork() ?: EthAmountAndValue(0.0, 0.0)
        }
    }

    override fun onCleared() {
        super.onCleared()
        closeHttpClientUseCase()
    }
}