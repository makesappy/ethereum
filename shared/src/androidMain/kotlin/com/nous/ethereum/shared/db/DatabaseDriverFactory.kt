package com.nous.ethereum.shared.db

import android.content.Context
import com.nous.ethereum.shared.cache.EthereumDatabase
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver

actual class DatabaseDriverFactory(
    private val context: Context
) {
    actual fun createDriver(): SqlDriver =
        AndroidSqliteDriver(EthereumDatabase.Schema, context, "ethereum.db")
}