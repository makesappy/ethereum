import SwiftUI
import shared

struct ContentView: View {
    let value: String
    
    var body: some View {
        VStack(spacing: 1){
            Text(value)
            Button("Set",action: {
                SetEthAmountAndValueUseCase().invoke(ethAmount: 2.07000493, ethValue: 0.0)
            } ).buttonStyle(SetButtonStyle())
            Button("Update"){}.buttonStyle(GetButtonStyle())
        }
    }
}

struct SetButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .foregroundColor(.white)
            .font(Font.body.bold())
            .frame(maxWidth: 50)
            .padding(.vertical, 10)
            .background(RoundedRectangle(cornerRadius: 8).fill(Color.green))
            .multilineTextAlignment(.center)
    }
}

struct GetButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .foregroundColor(.white)
            .font(Font.body.bold())
            .frame(maxWidth: 100)
            .padding(.vertical, 10)
            .background(RoundedRectangle(cornerRadius: 8).fill(Color.green))
            .multilineTextAlignment(.center)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(value: "loading...")
    }
}
