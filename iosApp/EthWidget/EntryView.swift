import WidgetKit
import SwiftUI

struct EntryView{
    let model: WidgetContent

    var body: some View {
      VStack(alignment: .leading) {
        Text(model.name)
          .font(.uiTitle4)
          .lineLimit(2)
          .fixedSize(horizontal: false, vertical: true)
          .padding([.trailing], 15)
          .foregroundColor(.titleText)
        
        Text(model.cardViewSubtitle)
          .font(.uiCaption)
          .lineLimit(nil)
          .foregroundColor(.contentText)
        
        Text(model.descriptionPlainText)
          .font(.uiCaption)
          .fixedSize(horizontal: false, vertical: true)
          .lineLimit(2)
          .lineSpacing(3)
          .foregroundColor(.contentText)
        
        Text(model.releasedAtDateTimeString)
          .font(.uiCaption)
          .lineLimit(1)
          .foregroundColor(.contentText)
      }
      .background(Color.cardBackground)
      .padding()
      .cornerRadius(6)
    }

}

@main
struct CommitCheckerWidget: Widget {
    private let kind: String = "CommitCheckerWidget"

    public var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: CommitTimeline(), placeholder: PlaceholderView()) { entry in
            CommitCheckerWidgetView(entry: entry)
        }
        .configurationDisplayName("Swift's Latest Commit")
        .description("Shows the last commit at the Swift repo.")
    }
}
