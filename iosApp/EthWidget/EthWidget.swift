//
//  EthWidget.swift
//  EthWidget
//
//  Created by Heorhii  Karpenko on 31.12.2020.
//  Copyright © 2020 orgName. All rights reserved.
//

import WidgetKit
import SwiftUI
import shared

struct Provider: TimelineProvider {
    func placeholder(in context: Context) -> SimpleEntry {
        return SimpleEntry(date: Date(),value: "Placeholder...")
    }

    func getSnapshot(in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let entry = SimpleEntry(date: Date(),value: "Snapshot...")
        completion(entry)
    }

    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        var entries: [SimpleEntry] = []

            // Generate a timeline consisting of five entries an hour apart, starting from the current date.
        let currentDate = Date()
        for minuteOffset in 0 ..< 6 {
            let entryDate = Calendar.current.date(byAdding: .minute, value: minuteOffset, to: currentDate)!
            let eth = GetEthAmountAndValueUseCase().doWork()
            let entry = SimpleEntry(date: entryDate,value: String(format: "%f", Int(eth?.amount ?? 0)))
            entries.append(entry)
        }

        let timeline = Timeline(entries: entries, policy: .atEnd)
        completion(timeline)
    }
}

struct SimpleEntry: TimelineEntry {
    let date: Date
    let value: String
}

struct EthWidgetEntryView : View {
    var entry: Provider.Entry

    var body: some View {
        Text(entry.value)
    }
}

@main
struct EthWidget: Widget {
    let kind: String = "EthWidget"

    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            EthWidgetEntryView(entry: entry)
        }
        .configurationDisplayName("My Widget")
        .description("This is an example widget.")
    }
}

struct EthWidget_Previews: PreviewProvider {
    static var previews: some View {
        EthWidgetEntryView(entry: SimpleEntry(date: Date(),value: "Preview..."))
            .previewContext(WidgetPreviewContext(family: .systemSmall))
    }
}
